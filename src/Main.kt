import tornadofx.launch
import view.TetrisApp

fun main() = launch<TetrisApp>()